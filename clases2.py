#!usr/bin/env python
# -*- coding: utf-8 -*-
class Pierna(object):
    piel=None
    grasa=None
    #Atributos privados
    __tamano=None

    #Metodos especiales. Constructor.
    def __init__(self,piel,grasa,tamano):
        self.piel=piel
        self.grasa=grasa
        if tamano>200:
            raise Exception("tamano no permitido")
        self.__tamano=tamano


    def __str__(self):
        return "piel "+str(self.piel)+", grasa"+str(self.grasa)+", tamano "+str(self.__tamano)


    def __repr__(self):
        return self.__str__()


class Humano():
    cabello=None
    brazos=[]
    piernas=[]
    cuerpo=None
    nivelProgramacion=0
    cerebro=None

    def dormir(self):
        return "El humano duerme"

   
    def comer(self):
        return "El humano come frutas y verduras"


    def programar(self):
        return "El humano programa en c"
     

    def nadar(self):
        return "El humano nada en de perrito"


    
